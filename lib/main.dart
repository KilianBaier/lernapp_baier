import 'package:flutter/material.dart';
import 'package:lernapp/studies.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home:  MyLernApp(title: 'Lern App - Kilian Baier'),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}


