import 'package:flutter/material.dart';

class Listtile extends StatefulWidget {
  final String fach;
  final DateTime dateTime;
  final double hours;
  final Function deleteItem;

  const Listtile({
    Key? key,
    required this.fach,
    required this.dateTime,
    required this.hours,
    required this.deleteItem,
  }) : super(key: key);

  @override
  _ListtileState createState() => _ListtileState();
}

class _ListtileState extends State<Listtile> {
  @override
  Widget build(BuildContext context) {
    final formattedDate =
        "${widget.dateTime.day}.${widget.dateTime.month}.${widget.dateTime.year}";

    return Card(
      color: Colors.white,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      elevation: 5,
      child: ListTile(
        leading: Container(
          width: 50,
          height: 50,
          alignment: Alignment.center,
          child: Text(
            widget.hours.toString() + "h",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          decoration: BoxDecoration(
            color: Colors.blue,
            border: Border.all(color: Colors.black, width: 1),
          ),
        ),
        title: Text(
          widget.fach,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Text(
          formattedDate,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.normal,
            color: Colors.grey[600],
          ),
        ),
        trailing: IconButton(
          icon: Icon(
            Icons.delete,
            size: 30,
          ),
          onPressed: () => widget.deleteItem(this.widget),
        ),
      ),
    );
  }
}
