import 'package:flutter/material.dart';

class Bar extends StatelessWidget {
  final double percentValue;
  final String subject;
  final double value;

  const Bar({
    Key? key,
    required this.percentValue,
    required this.subject,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Text(subject),
            Text(value.toString() + "h"),
            Stack(children: [
              RotatedBox(
                quarterTurns: 1,
                child: Container(
                  height: 20,
                  width: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.black, width: 1),
                  ),
                ),
              ),
              Positioned(
                bottom: 1,
                left: 1,
                child: RotatedBox(
                  quarterTurns: 1,
                  child: Container(
                    height: 18,
                    width: percentValue,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: percentValue >= 195 || percentValue <= 5
                          ? BorderRadius.circular(10)
                          : BorderRadius.only(
                              bottomRight: Radius.circular(10),
                              topRight: Radius.circular(10),
                            ),
                    ),
                  ),
                ),
              ),
            ]),
          ],
        ),
      ),
    );
  }
}
