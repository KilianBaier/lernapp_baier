import 'dart:collection';

import 'package:flutter/material.dart';

import 'Listtile.dart';
import 'bar.dart';
import 'dropDowm.dart';

class MyLernApp extends StatefulWidget {
  final String title;

  MyLernApp({required this.title});

  @override
  State<MyLernApp> createState() => _MyLernApp();
}

class _MyLernApp extends State<MyLernApp> {
  double currentSliderValue = 0;
  final TextEditingController stundenController = TextEditingController();
  String dropdownValue = "";
  List<Listtile> list = [];
  Map<String, double> myMap = {};
  List<Bar> barList = [];
  double sumValue = 0;
  bool showModalBottom = false;
  DateTime dateTime = DateTime.now();

  void deleteItem(Listtile item) {
    setState(() {
      list.remove(item);
    });
    if (myMap.containsKey(item.fach)) {
      myMap[item.fach] = myMap[item.fach]! - item.hours;
      if (myMap[item.fach]! <= 0) {
        myMap.remove(item.fach);
      }
    }
    updateSumValue();
    lastLength = lastLength - 1;
    getListeTileData();
  }

  void sortBarList() {
    barList.sort((a, b) => b.value.compareTo(a.value));
  }

  void getDropDown(String value) {
    setState(() {
      dropdownValue = value;
    });
  }

  int lastLength = 0;

  void updateSumValue() {
    sumValue = 0;
    for (int i = 0; i < list.length; i++) {
      sumValue += list[i].hours;
      print(sumValue);
    }
  }

  void getListeTileData() {
    for (int i = lastLength; i < list.length; i++) {
      if (myMap.containsKey(list[i].fach)) {
        myMap[list[i].fach] = myMap[list[i].fach]! + list[i].hours;
      } else {
        myMap[list[i].fach] = list[i].hours;
      }
      lastLength = list.length;
    }
    listOfBar();
  }

  bool checkIfStringIsNumber(String value) {
    try {
      double.parse(value);
      return true;
    } catch (e) {
      return false;
    }
  }

  void listOfBar() {
    barList.clear();
    for (int i = 0; i < myMap.length; i++) {
      barList.add(Bar(
        subject: myMap.keys.elementAt(i),
        percentValue: ((myMap.values.elementAt(i) / sumValue) * 200),
        value: myMap.values.elementAt(i),
      ));
    }
    sortBarList();
  }

  Widget _buildListView() {
    if (list.length > 0) {
      return Expanded(
        child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return list[index];
          },
        ),
      );
    } else {
      return Expanded(
        child: Center(
          child: Text("Time to study!"),
        ),
      );
    }
  }

  Widget _buildBarListView() {
    return Expanded(
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: barList.length,
        itemBuilder: (context, index) {
          return barList[index];
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          barList.length > 0 ? _buildBarListView() : Container(),
          _buildListView(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showModalBottomSheet(
            context: context,
            builder: (BuildContext context) {
              return Container(
                child: Column(
                  children: [
                    DropDown(
                      getDropDown: (value) => {getDropDown(value)},
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 100.0, right: 100.0, top: 8.0),
                      child: TextField(
                        controller: stundenController,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            borderSide: BorderSide(color: Colors.grey),
                          ),
                          labelText: 'Stunden',
                          labelStyle: TextStyle(color: Colors.grey),
                          hintText: 'Geben Sie die Stunden ein',
                          hintStyle: TextStyle(color: Colors.grey),
                          fillColor: Colors.white,
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 16.0),
                        ),
                        style: TextStyle(fontSize: 16.0, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: () => {
                            if ((stundenController.text != "" ||
                                    checkIfStringIsNumber(
                                        stundenController.text)) &&
                                (dropdownValue != ""))
                              {
                                setState(() {
                                  list.add(Listtile(
                                      fach: dropdownValue,
                                      dateTime: dateTime,
                                      hours:
                                          double.parse(stundenController.text),
                                      deleteItem: (listItem) =>
                                          deleteItem(listItem)));
                                  updateSumValue();
                                  stundenController.clear();
                                })
                              }
                            else
                              null,
                            getListeTileData(),
                            Navigator.pop(context),
                          },
                          child: Text("+"),
                          style: ElevatedButton.styleFrom(primary: Colors.blue),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () async {
                        final DateTime? currentDate = await showDatePicker(
                          context: context,
                          initialDate: DateTime.now(),
                          firstDate: DateTime(2022),
                          lastDate: DateTime.now(),
                        );
                        if (currentDate != null && currentDate != dateTime) {
                          setState(() {
                            dateTime = currentDate;
                          });
                        }
                      },
                      child: Text(
                        "${dateTime.day}.${dateTime.month}.${dateTime.year}",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.blue,
      ),
    );
  }
}
