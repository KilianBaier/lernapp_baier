import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DropDown extends StatefulWidget {

  final Function getDropDown;
  const DropDown({Key? key, required this.getDropDown}) : super(key: key);

  @override
  _DropDownState createState() => _DropDownState();
}

class _DropDownState extends State<DropDown> {
  String? _selectedItem;

  List<String> items = <String>[
    'POS',
    'BWM',
    'MATHE',
    'GGP',
    'NVS',
    'REL',
    'ENG',
    'DBI'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const Text(
              'Auswählen',
              style: TextStyle(fontSize: 16.0),
            ),
          ),
          DropdownButton<String>(
            value: _selectedItem,
            icon: const Icon(Icons.arrow_drop_down),
            onChanged: (String? newValue) {
              setState(() {
                print(_selectedItem);
                _selectedItem = newValue;
              });
              widget.getDropDown(_selectedItem);
            },
            items: items.map((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
            dropdownColor: Colors.white,
            style: TextStyle(color: Colors.black),
          ),
        ],
      ),
    );
  }
}
